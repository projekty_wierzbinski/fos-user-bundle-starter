<?php

namespace UserBundle\Controller;


use UserBundle\Entity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('UserBundle:Parts:dashboard.html.twig');
    }
    public function newOrderAction()
    {
        $form = $this->createFormBuilder()
            ->add('username', 'text', array(
                'attr' => array('class'=>'form-control', 'placeholder' => 'Wpisz nazwę użytkownika')
            ))
            ->add('password', 'text', array(
                'attr' => array('class'=>'form-control', 'placeholder' => 'Wpisz lub wygeneruj hasło')
            ))
            ->add('save', 'submit')
            ->getForm();

        if ($form->isValid()) {
            
        }

        return $this->render('UserBundle:Parts:add_user.html.twig', array('form' => $form->createView()));

    }
}
